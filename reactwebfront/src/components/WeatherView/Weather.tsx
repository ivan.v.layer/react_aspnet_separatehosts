import { IWeather } from "../../models/models";

interface WeatherProps {

    weather: IWeather
}

export function Weather({weather} : WeatherProps)
{
    var weatherDate = new Date(weather.date);
    const options : Intl.DateTimeFormatOptions  = {year: 'numeric', month: '2-digit',day: '2-digit'};

    return(
        <div className="border rounded py-5 px-5 items-center">

            <p>Дата: {weatherDate.toLocaleDateString("ru-RU",options)}</p>
            <p>Температура воздуха: {weather.temperatureC}</p>
            <p>Ощущение: {weather.summary}</p>
        </div>
    )
}