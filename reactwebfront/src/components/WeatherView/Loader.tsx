import React from 'react'

export function Loader() {
  return (
    <p className="text-center">Подождите, загружаем информацию о погоде...</p>
  )
}