import { useSupportWeather } from '../../hooks/useSupportWeather'
import {Weather} from './Weather'
import {Loader} from './Loader'


export function WeathersView()
{
    const {weathers,loading, error} = useSupportWeather()

    return(
        <>
        { loading && <Loader /> }
        {error && <p className="text-red-700">{error} </p>}    
        <p className="py-5">Прогноз погоды:</p>
        <div className="container flex justify-between py-5 px-5 center">
            {weathers.map(weather => <Weather weather={weather} key={crypto.randomUUID()}/>)}
        </div>
        </>
    )

}