import axios, { AxiosError } from 'axios'
import { IWeather } from '../models/models'
import { useEffect, useState } from 'react'

export function useSupportWeather() {
    const [weathers, setWeathers] = useState<IWeather[]>([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState('')


    async function fetchWeather() {
        try {
            setError('')
            setLoading(true)
            console.log('Test')

            const response = await axios.get<IWeather[]>('http://localhost:5000/WeatherForecast')
            setWeathers(response.data)
            console.log(response.data);
            setLoading(false)
        } catch (e: unknown) {
            const error = e as AxiosError
            setLoading(false)
            setError(error.message)
        }
    }

    useEffect(() => {
        fetchWeather()
    }, [])

    return { weathers , loading, error}
}

