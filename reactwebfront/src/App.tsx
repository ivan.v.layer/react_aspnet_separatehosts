import React from 'react';
import './App.css';
import {WeathersView} from './components/WeatherView/WeathersView'

function App() {
  return (
    <div className="App">
      <WeathersView />
    </div>
  );
}

export default App;
